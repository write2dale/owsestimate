﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace Solar
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://owssolarestimate.azurewebsites.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        //******************************************************* START ROCKY MOUNTAIN ********************************************************
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RockyMountainJSON RockyMountain(string username, string password)
        {
            //https://www.rockymountainpower.net/identity/login
            CookieContainer cookieJar = new CookieContainer();

            //populate cookies
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.rockymountainpower.net/index.html");
            request.Method = "GET";
            request.CookieContainer = cookieJar;
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            string result = reader.ReadToEnd();
           
            //while no data
            //wait 15 secs
            //Thread.Sleep(15000);

            //cams_original_url=http%3A%2F%2Fwww.rockymountainpower.net%2Fya%2Fvpb%2Fuh.wcss.html&cams_login_config=http&cams_security_domain=system&cams_cb_username=lehiwilkerson&cams_cb_password=asdfgh1
            request = (HttpWebRequest)WebRequest.Create("https://www.rockymountainpower.net/identity/login");
            request.Method = "POST";
            request.CookieContainer = cookieJar;
            request.ContentType = "application/x-www-form-urlencoded";
            string postData = string.Format("cams_original_url=http%3A%2F%2Fwww.rockymountainpower.net%2Fya%2Fvpb%2Fuh.wcss.html&cams_login_config=http&cams_security_domain=system&cams_cb_username={0}&cams_cb_password={1}", username, password);
            //usagHistoryForSite2=usagHistoryForSite2&usagHistoryForSite2%3AusagePeriodRadioBtn=24&javax.faces.ViewState=j_id5
            //&usagHistoryForSite2%3AusagePeriodRadioBtn=24
            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = bytes.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);

            response = request.GetResponse();
            stream = response.GetResponseStream();
            reader = new StreamReader(stream);

            result = reader.ReadToEnd();

            //wait 5 secs
         Thread.Sleep(5500);

            //get usage
            //https://www.rockymountainpower.net/ya/vpb/uh.wcss.html
            request = (HttpWebRequest)WebRequest.Create("https://www.rockymountainpower.net/ya/vpb/uh.wcss.html");
            request.Method = "GET";
            request.CookieContainer = cookieJar;
            response = request.GetResponse();
            stream = response.GetResponseStream();
            reader = new StreamReader(stream);
            result = reader.ReadToEnd();

            string jid = Regex.Match(result, "id=\"javax.faces.ViewState\" value=\"(j_id\\d+)\"").Groups[1].Value;
            request = (HttpWebRequest)WebRequest.Create("https://www.rockymountainpower.net/ya/yourBill/usageHistory.wcss.html");
            request.Method = "POST";
            request.CookieContainer = cookieJar;
            request.ContentType = "application/x-www-form-urlencoded";
            postData = "usagHistoryForSite2=usagHistoryForSite2&usagHistoryForSite2%3AusagePeriodRadioBtn=24&javax.faces.ViewState=" + jid;
            bytes = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = bytes.Length;

            requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);

            response = request.GetResponse();
            stream = response.GetResponseStream();
            reader = new StreamReader(stream);

            result = reader.ReadToEnd();


            Match nameMatch = Regex.Match(result, "<strong class=\"name\">(.*?)</strong>\\s*<div class=\"collapsible\">\\s*<p>(.*?)<br\\s/>(.*?),\\s(.*?)\\s(.*?)<br\\s/>(.*?)<br\\s/>(.*?)\\s");
            Match accountMatch = Regex.Match(result, "<strong class=\"account\">(.*?)<");
            Match meterMatch = Regex.Match(result, "Meter# (.*?)\\)");
            MatchCollection usageMatches = Regex.Matches(result, "<td>(.*?)</td>\\s*<td class=\"numeric\">(.*?)</td>\\s*<td class=\"numeric\">(.*?)</td>\\s*<td class=\"currency\">\\$(.*?)<");

            //so do your regex online. when it matches paste int, and then escape the "'s
            // which are easy to ee because of the black.. did you notice? these guys two the red squiggles, cause " is a special char, and so is \, in c# :) do the black first
            //then do \, either way, escape the same wayk

            RockyMountainJSON rmj = new RockyMountainJSON();
            rmj.customerName = nameMatch.Groups[1].Value;
            rmj.customerAddress = nameMatch.Groups[2].Value;
            rmj.customerCity = nameMatch.Groups[3].Value;
            rmj.customerState = nameMatch.Groups[4].Value;
            rmj.customerZip = nameMatch.Groups[5].Value;
            rmj.customerPhone = nameMatch.Groups[6].Value;
            rmj.customerEmail = nameMatch.Groups[7].Value;
            rmj.customerAccount = accountMatch.Groups[1].Value;
            rmj.customerMeter = meterMatch.Groups[1].Value;

            foreach (Match m in usageMatches)
            {
                UsageData ud = new UsageData();
                ud.date = m.Groups[1].Value;
                ud.days = m.Groups[2].Value;
                ud.kw = m.Groups[3].Value;
                ud.cost = m.Groups[4].Value;

                rmj.usage.Add(ud);
            }

            Match graphMatch = Regex.Match(result, "usageGraph\" src=\"(.*?)\"");
            string url = "https://www.rockymountainpower.net" + graphMatch.Groups[1].Value;
            WebClient wc = new WebClient(); // i DON'T THINK wc is being used

            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            httpWebRequest.CookieContainer = cookieJar;
            using (HttpWebResponse httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse())
            {
                //graphs might not exist.. it didn't on mine when i got your project.. so two thigns..

                using (stream = httpWebReponse.GetResponseStream())
                {
                    Image.FromStream(stream).Save(Server.MapPath("Content/graphs/" + rmj.customerMeter + ".jpg"));
                }
            }

            return rmj;
        }
        //******************************************************* End OF ROCKYMOUNTAIN ********************************************************

        //******************************************************* START PROVO POWER ********************************************************
        //these thigns are called attributes, a way of decoraiting a method.. needed for the webservice to know what the method is.. [WebMethod] tells the ws
        //that THIS method isn't a normal method, but somethign to expose to the outside. 
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RockyMountainJSON ProvoPower(string username, string password)
        {
            //https://www.onlinebiller.com/provocity/
            CookieContainer cookieJar = new CookieContainer();

            //populate cookies
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.onlinebiller.com/provocity/index.html");
            request.Method = "GET";
            request.CookieContainer = cookieJar;
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            string result = reader.ReadToEnd();

            //here you grab pst value from result
            Match pstmatch = Regex.Match(result, "name=\"pst\" value=\"(.*?)\"");
            string pst = pstmatch.Groups[1].Value;

            //pst=g7jukt4b17d7n&principal=write2dale&password=Asdfghjk1&submitbtn=+Login+
            request = (HttpWebRequest)WebRequest.Create("https://www.onlinebiller.com/provocity/login_submit.html");
            request.Method = "POST";
            request.CookieContainer = cookieJar;
            request.ContentType = "application/x-www-form-urlencoded";
            string postData = string.Format("pst=" + pst + "&principal=" + username + "&password=" + password + "&submitbtn=+Login+");

            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = bytes.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);

            response = request.GetResponse();
            stream = response.GetResponseStream();
            reader = new StreamReader(stream);

            result = reader.ReadToEnd();

            //wait 4 secs
            //Thread.Sleep(5000);

            ////get usage
            ////https://www.onlinebiller.com/provocity/index.html
            request = (HttpWebRequest)WebRequest.Create("https://www.onlinebiller.com/provocity/statements.html");
            request.Method = "GET";
            request.CookieContainer = cookieJar;
            response = request.GetResponse();
            stream = response.GetResponseStream();
            reader = new StreamReader(stream);
            result = reader.ReadToEnd();

            //wait 4 secs
            //Thread.Sleep(5000);

            RockyMountainJSON rmj = new RockyMountainJSON();

            // <td class="\w*">Statement<\/td>\s*<td class="\w*">(.*?)<.*?doc_token=(.*?)'
            //grab date and doc_token values from result (pdf's to download)
            MatchCollection doctokenmatch = Regex.Matches(result, "<td class=\"\\w*\">Statement<\\/td>\\s*<td class=\"\\w*\">(.*?)<.*?doc_token=(.*?)'", RegexOptions.Singleline);
            bool first = true;
            foreach (Match d in doctokenmatch)
            {
                //save pdfs as username_date
                //            https://www.onlinebiller.com/provocity/download_document_v2.html?doc_token=In9cMcjpasYcj2Dowct9D2Fe
                string url = "https://www.onlinebiller.com/provocity/download_document_v2.html?doc_token=" + d.Groups[2].Value;
                HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                httpWebRequest.CookieContainer = cookieJar;
                using (HttpWebResponse httpWebReponse = (HttpWebResponse)httpWebRequest.GetResponse())
                {
                    using (stream = httpWebReponse.GetResponseStream())
                    {
                        using (var fileStream = new FileStream(Server.MapPath("Content/graphs/" + username + "_" + d.Groups[1].Value + ".pdf"), FileMode.Create, FileAccess.Write))
                        {
                            stream.CopyTo(fileStream);
                        }

                        //the stream is pdf, so we'll need to convert to read result
                        //for now just manually put it in
                        StringBuilder text = new StringBuilder();
                        if (File.Exists(Server.MapPath("Content/graphs/" + username + "_" + d.Groups[1].Value + ".pdf")))
                        {
                            PdfReader pdfReader = new PdfReader(Server.MapPath("Content/graphs/" + username + "_" + d.Groups[1].Value + ".pdf"));

                            for (int page = 1; page <= pdfReader.NumberOfPages; page++)
                            {
                                ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                                string currentText = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy);

                                currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
                                text.Append(currentText);
                            }

                            //wait 1 secs
                            //Thread.Sleep(2000);

                            //parse this one pdf txt for data

                            //if this is the first pdf get the customer info once
                            if (first)
                            {
                                // Help Contribution:\n(.*?)\n(.*?)\n(.*?)\s(.*?)\s(.*?)\n.*?KEY=(.*?)\*Index.*?Electric Meter # (.*?)\n
                                Match nameMatch = Regex.Match(text.ToString(), "Help Contribution:\n(.*?)\n(.*?)\n(.*?)\\s(.*?)\\s(.*?)\n.*?KEY=(.*?)\\*Index.*?Electric Meter # (.*?)\n", RegexOptions.Singleline);
                                rmj.customerName = nameMatch.Groups[1].Value;
                                rmj.customerAddress = nameMatch.Groups[2].Value;
                                rmj.customerCity = nameMatch.Groups[3].Value;
                                rmj.customerState = nameMatch.Groups[4].Value;
                                rmj.customerZip = nameMatch.Groups[5].Value;
                                //rmj.customerPhone = nameMatch.Groups[8].Value; //does this site have email or phone# availible? on bill anywhere?
                                //rmj.customerEmail = nameMatch.Groups[9].Value;
                                rmj.customerAccount = nameMatch.Groups[6].Value;
                                rmj.customerMeter = nameMatch.Groups[7].Value;
                                first = false;
                            }
                            // grab the data from bill
                            //Last\nMonth\nLast\nYear\n.*?\n.*?\n(.*?)\s(.*?)\s.*?\s(.*?)\nkWh
                            Match mdata = Regex.Match(text.ToString(), "Last\nMonth\nLast\nYear\n.*?\n.*?\n(.*?)\\s(.*?)\\s.*?\\s(.*?)\nkWh");  //should have use (\d+) or (\d+\.\d+) or [\d\.]+ for numbers instead of (.*?)

                            string monthcost = (float.Parse(mdata.Groups[2].Value) * float.Parse(mdata.Groups[1].Value)).ToString(); // averageDaylyCost * NumberOfDays = MonthlyCost

                            //add the results to object
                            UsageData ud = new UsageData();
                            ud.date = d.Groups[1].Value;    // date from d.Groups above, others from mdata.Groups // like     Nov  3, 2014
                            ud.days = mdata.Groups[1].Value;
                            ud.kw = mdata.Groups[3].Value;
                            ud.cost = monthcost;

                            rmj.usage.Add(ud);

                            pdfReader.Close();

                        }

                    }
                }
            }

            //send object back to front end
            return rmj;
        }
        //******************************************************* End OF PROVO POWER ********************************************************

        //******************************************************* START Springville Power ********************************************************
        //these thigns are called attributes, a way of decoraiting a method.. needed for the webservice to know what the method is.. [WebMethod] tells the ws
        //that THIS method isn't a normal method, but somethign to expose to the outside. 
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public RockyMountainJSON SpringvillePower(string username, string password)
        {
            //https://www.xpressbillpay.com/
            CookieContainer cookieJar = new CookieContainer();

            //populate cookies
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://www.xpressbillpay.com/index.php");
            request.Method = "GET";
            request.CookieContainer = cookieJar;
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            string result = reader.ReadToEnd();

            //here you grab CSRFName and CSRFToken value from result
            Match CSRFinfo = Regex.Match(result, "name='CSRFTime' value='(.*?)'.*?name='CSRFName' value='(.*?)'.*?name='CSRFToken' value='(.*?)'", RegexOptions.Singleline);
            string CSRFTime = CSRFinfo.Groups[1].Value;
            string CSRFName = CSRFinfo.Groups[2].Value;
            string CSRFToken = CSRFinfo.Groups[3].Value;

            //CSRFTime=1415650194 &CSRFName=CSRFGuard_949522061 &CSRFToken=af93862bd9265a937c4d &login=thaabit%40gmail.com &password=FZ18ti45IgCzqX91ukcN &Submit=Login+%3E%3E
            request = (HttpWebRequest)WebRequest.Create("https://www.xpressbillpay.com/utils/custlogin.php");
            request.Method = "POST";
            request.CookieContainer = cookieJar;
            request.ContentType = "application/x-www-form-urlencoded";
            string postData = string.Format("CSRFTime=" + CSRFTime + "&CSRFName=" + CSRFName + "&CSRFToken=" + CSRFToken + "&login=" + username + "&password=" + password + "&Submit=Login+%3E%3E");

            byte[] bytes = Encoding.UTF8.GetBytes(postData);
            request.ContentLength = bytes.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);

            response = request.GetResponse();
            stream = response.GetResponseStream();
            reader = new StreamReader(stream);
            result = reader.ReadToEnd();

            ////get url list bill page
            // Request URL:https://www.xpressbillpay.com/cust/account_entities.php?from=login&CSRFName=CSRFGuard_708284343&CSRFToken=506eb7dccfc043250d32
            request = (HttpWebRequest)WebRequest.Create("https://www.xpressbillpay.com/common/payment_history.php");
            request.Method = "GET";
            request.CookieContainer = cookieJar;
            response = request.GetResponse();
            stream = response.GetResponseStream();
            reader = new StreamReader(stream);
            result = reader.ReadToEnd();
            // <td class="\w*">Statement<\/td>\s*<td class="\w*">(.*?)<.*?doc_token=(.*?)'
            //grab date and url for bills
            MatchCollection doctokenmatch = Regex.Matches(result, "href=\"bill_disp\\/index.php\\?ebill_id=(.*?)\"", RegexOptions.Singleline);

            RockyMountainJSON rmj = new RockyMountainJSON();
            bool first = true;
            //get data from each bill
            foreach (Match d in doctokenmatch)
            {
                //            https://www.xpressbillpay.com/common/bill_disp/index.php?ebill_id=NDUyNDU3OTgzNzcy&CSRFName=CSRFGuard_1594698495&CSRFToken=bffff494a
                string url = "https://www.xpressbillpay.com/common/bill_disp/index.php?ebill_id=" + d.Groups[1].Value;

                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.CookieContainer = cookieJar;
                response = request.GetResponse();
                stream = response.GetResponseStream();
                reader = new StreamReader(stream);
                result = reader.ReadToEnd();

                //if this is the first bill get the customer info once
                if (first) 
                {
                    // Billing Address<\/td>.*?<td class="body_text">(.*?),\s(.*?)<.*?<td class="body_text">(.*?)<.*?<td class="body_text">(.*?)<.*?<td class="body_text">(.*?),\s(.*?)\s(.*?)<.*?<td class="body_text">(.*?)<.*?<td class="body_text">(.*?)<.*?<td class="body_text">(.*?)<.*?<td class="body_text">(.*?)<
                    Match infoMatch = Regex.Match(result.ToString(), "Billing Address<\\/td>.*?<td class=\"body_text\">(.*?),\\s(.*?)<.*?<td class=\"body_text\">(.*?)<.*?<td class=\"body_text\">(.*?)<.*?<td class=\"body_text\">(.*?),\\s(.*?)\\s(.*?)<.*?<td class=\"body_text\">(.*?)<.*?<td class=\"body_text\">(.*?)<.*?<td class=\"body_text\">(.*?)<.*?<td class=\"body_text\">(.*?)<", RegexOptions.Singleline);
                    string customerFullName = infoMatch.Groups[2].Value + " " + infoMatch.Groups[1].Value;
                    rmj.customerName = customerFullName;
                    rmj.customerAddress = infoMatch.Groups[3].Value;
                    rmj.customerCity = infoMatch.Groups[5].Value;
                    rmj.customerState = infoMatch.Groups[6].Value;
                    rmj.customerZip = infoMatch.Groups[7].Value;
                    //rmj.customerPhone = infoMatch.Groups[8].Value; //does this site have email or phone# availible? on bill anywhere?
                    //rmj.customerEmail = infoMatch.Groups[9].Value;
                    rmj.customerAccount = infoMatch.Groups[11].Value;
                    //rmj.customerMeter = infoMatch.Groups[7].Value; //does this site have meter # availible?
                    first = false;
                }

                            // parse the data from this one bill
                           //Total Usage.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?<td align="center">(.*?)<.*?ELECTRIC.*?\$(.*?)<.*?ENERGY .*?\$(.*?)<
                Match mdata = Regex.Match(result.ToString(), "Total Usage.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?<td align=\"center\">(.*?)<.*?ELECTRIC.*?\\$(.*?)<.*?ENERGY .*?\\$(.*?)<", RegexOptions.Singleline);  //should have use (\d+) or (\d+\.\d+) or [\d\.]+ for numbers instead of (.*?)
                            string monthlyCost = (float.Parse(mdata.Groups[11].Value)  + float.Parse(mdata.Groups[12].Value)).ToString();
                            DateTime dt = Convert.ToDateTime(mdata.Groups[7].Value);  
                            string mdate = dt.ToString("MMM dd yyyy");
                            //add the results to object
                            UsageData ud = new UsageData();
                            ud.date = mdate;
                            ud.days = "30.4";
                            ud.kw = mdata.Groups[10].Value;
                            ud.cost = monthlyCost;

                            rmj.usage.Add(ud);
            }
            //send object back to front end
            return rmj;
        }
        //******************************************************* End OF Springville Power ********************************************************


}

    public class RockyMountainJSON
    {

        public string customerName;
        public string customerAddress;
        public string customerCity;
        public string customerState;
        public string customerZip;
        public string customerPhone;
        public string customerEmail;
        public string customerAccount;
        public string customerMeter;

        public List<UsageData> usage = new List<UsageData>();
        //add the somethingelse to here cause this is the return object we're packaging up
    }

    public class UsageData
    {
        public string date;
        public string days;
        public string kw;
        public string cost;
    }

    //public class DocData
    //{
    //    public string date;
    //    public string token;

    //}

}
