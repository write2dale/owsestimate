﻿var RockyMountainJSON;
var geocoder;
var map;

$(function () {
    initialize();

    $('#SiteUsername').val("thaabit@gmail.com");
    $('#SitePassword').val("FZ18ti45IgCzqX91ukcN");

    $('#savecheck').on('click', function () {
        $(this).toggleClass("blue red");
        save();
    });

    $('.smallTB').on('input', function () {
        calculate();
    });

    $('.UsageTotals').on('input', function () {
        CalcSystemsize();
    });

    //$('#kWSystemSize').on('input', function () {
    //    CalcPanels();
    //});

    $('#NumberofPanels').on('input', function () {
        $('#kWSystemSize').val((($('#NumberofPanels').val() * $('#Panelwatts').val())) / 1000);
        CalcPanels();
    });

    $('.arrayData').on('input', function () {
        CalcPanels();
    });

    $('a[href="#solararray"]').on('click', function () {    //solarArray tab is selected
        if ($('#CustomerAddress').val() != "" || $('#CustomerCity').val() != "" || $('#Customerstate').val() != "") {
            codeAddress();
        }
        $('#googmapimg').attr("src", "https://maps.googleapis.com/maps/api/staticmap?center=" + $('#CustomerAddress').val() + "," + $('#CustomerCity').val() + "," + $('#Customerstate').val() + "," + $('#CustomerZip').val() + "&zoom=20&size=230x160&maptype=satellite");
        var w = window.innerWidth;
    })
});

function calculate() {
    var DaysTotal1 = 0, DaysCount1 = 0, KwsTotal1 = 0, KwsCount1 = 0, CostsTotal1 = 0, CostsCount1 = 0;

    //add monthly totals year 1
    $('.monthdays1').each(function () {
        if (isNaN(parseFloat(this.value)) || this.value == 0) { this.value = ""; }
        else { DaysCount1 += 1; DaysTotal1 += parseFloat(this.value); }
    });

    $('.monthkw1').each(function () {
        if (isNaN(parseFloat(this.value)) || this.value == 0) { this.value = ""; }
        else { KwsCount1 += 1; KwsTotal1 += parseFloat(this.value); }
    });

    $('.monthcost1').each(function () {
        if (isNaN(parseFloat(this.value)) || this.value == 0) { this.value = ""; }
        else { CostsCount1 += 1; CostsTotal1 += parseFloat(this.value); }
    });

    //show monthly totals with normal rounding
    if ((isNaN(DaysTotal1)) || (DaysTotal1 == 0)) { $('#DaysTotal1').val(""); } else { $('#DaysTotal1').val(Math.ceil(DaysTotal1)); }
    if ((isNaN(KwsTotal1)) || (KwsTotal1 == 0)) { $('#KwsTotal1').val(""); } else { $('#KwsTotal1').val(Math.ceil(KwsTotal1)); }
    if ((isNaN(CostsTotal1)) || (CostsTotal1 == 0)) { $('#CostsTotal1').val(""); } else { $('#CostsTotal1').val(Math.ceil(CostsTotal1)); }
    //calc monthly averages with normal rounding
    if (isNaN(DaysTotal1 / DaysCount1) || ((DaysTotal1 / DaysCount1 == 0))) { $('#DaysAvg1').val(""); } else { $('#DaysAvg1').val(Math.ceil((DaysTotal1 / DaysCount1))); }
    if (isNaN(KwsTotal1 / KwsCount1) || ((KwsTotal1 / KwsCount1) == 0)) { $('#KwsAvg1').val(""); } else { $('#KwsAvg1').val(Math.ceil((KwsTotal1 / KwsCount1))); }
    if (isNaN(CostsTotal1 / CostsCount1) || ((CostsTotal1 / CostsCount1) == 0)) { $('#CostsAvg1').val(""); } else { $('#CostsAvg1').val(Math.ceil((CostsTotal1 / CostsCount1))); }

    //EstYr
    if ((isNaN(DaysTotal1)) || (DaysTotal1 == 0)) { $('#EstYrDaysTotal1').val(""); } else { $('#EstYrDaysTotal1').val("365"); }
    if ((isNaN(DaysTotal1)) || (DaysTotal1 == 0)) { $('#EstYrKwsTotal1').val(""); } else { $('#EstYrKwsTotal1').val(Math.ceil((parseFloat($('#EstYrDaysTotal1').val()) / $('#DaysTotal1').val()) * ($('#KwsTotal1').val()))); }
    if ((isNaN(DaysTotal1)) || (DaysTotal1 == 0)) { $('#EstYrKwsTotal1').val(""); } else { $('#EstYrCostsTotal1').val(Math.ceil((parseFloat($('#EstYrDaysTotal1').val()) / $('#DaysTotal1').val()) * ($('#CostsTotal1').val()))); }

    //calc daily average and average watt cost
    if (isNaN(KwsTotal1 / DaysTotal1) || ((KwsTotal1 / DaysTotal1) == 0) || !isFinite(KwsTotal1 / DaysTotal1)) { $('#kWDailyAverage1').val(""); } else { $('#kWDailyAverage1').val((KwsTotal1 / DaysTotal1).toFixed(2)); }
    if (isNaN(CostsTotal1 / KwsTotal1) || ((CostsTotal1 / KwsTotal1) == 0) || !isFinite(CostsTotal1 / KwsTotal1)) { $('#UtilityWattCost1').val(""); } else { $('#UtilityWattCost1').val((CostsTotal1 / KwsTotal1).toFixed(2)); }


    //_____________________________________________________________________________________________________
    //add monthly totals year 2
    var DaysTotal2 = 0, DaysCount2 = 0, KwsTotal2 = 0, KwsCount2 = 0, CostsTotal2 = 0, CostsCount2 = 0;

    $('.monthdays2').each(function () {
        if (isNaN(parseFloat(this.value)) || this.value == 0) { this.value = ""; }
        else { DaysCount2 += 1; DaysTotal2 += parseFloat(this.value); }
    });

    $('.monthkw2').each(function () {
        if (isNaN(parseFloat(this.value)) || this.value == 0) { this.value = ""; }
        else { KwsCount2 += 1; KwsTotal2 += parseFloat(this.value); }
    });

    $('.monthcost2').each(function () {
        if (isNaN(parseFloat(this.value)) || this.value == 0) { this.value = ""; }
        else { CostsCount2 += 1; CostsTotal2 += parseFloat(this.value); }
    });

    //show monthly totals with normal rounding
    if ((isNaN(DaysTotal2)) || (DaysTotal2 == 0)) { $('#DaysTotal2').val(""); } else { $('#DaysTotal2').val(Math.ceil(DaysTotal2)); }
    if ((isNaN(KwsTotal2)) || (KwsTotal2 == 0)) { $('#KwsTotal2').val(""); } else { $('#KwsTotal2').val(Math.ceil(KwsTotal2)); }
    if ((isNaN(CostsTotal2)) || (CostsTotal2 == 0)) { $('#CostsTotal2').val(""); } else { $('#CostsTotal2').val(Math.ceil(CostsTotal2)); }
    //calc monthly averages with normal rounding
    if (isNaN(DaysTotal2 / DaysCount2) || ((DaysTotal2 / DaysCount2 == 0))) { $('#DaysAvg2').val(""); } else { $('#DaysAvg2').val(Math.ceil((DaysTotal2 / DaysCount2))); }
    if (isNaN(KwsTotal2 / KwsCount2) || ((KwsTotal2 / KwsCount2) == 0)) { $('#KwsAvg2').val(""); } else { $('#KwsAvg2').val(Math.ceil((KwsTotal2 / KwsCount2))); }
    if (isNaN(CostsTotal2 / CostsCount2) || ((CostsTotal2 / CostsCount2) == 0)) { $('#CostsAvg2').val(""); } else { $('#CostsAvg2').val(Math.ceil((CostsTotal2 / CostsCount2))); }

    //EstYr
    if ((isNaN(DaysTotal2)) || (DaysTotal2 == 0)) { $('#EstYrDaysTotal2').val(""); } else { $('#EstYrDaysTotal2').val("365"); }
    if ((isNaN(DaysTotal2)) || (DaysTotal2 == 0)) { $('#EstYrKwsTotal2').val(""); } else { $('#EstYrKwsTotal2').val(Math.ceil((parseFloat($('#EstYrDaysTotal2').val()) / $('#DaysTotal2').val()) * ($('#KwsTotal2').val()))); }
    if ((isNaN(DaysTotal2)) || (DaysTotal2 == 0)) { $('#EstYrKwsTotal2').val(""); } else { $('#EstYrCostsTotal2').val(Math.ceil((parseFloat($('#EstYrDaysTotal2').val()) / $('#DaysTotal2').val()) * ($('#CostsTotal2').val()))); }

    //calc daily average and average watt cost
    if (isNaN(KwsTotal2 / DaysTotal2) || ((KwsTotal2 / DaysTotal2) == 0) || !isFinite(KwsTotal2 / DaysTotal2)) { $('#kWDailyAverage2').val(""); } else { $('#kWDailyAverage2').val((KwsTotal2 / DaysTotal2).toFixed(2)); }
    if (isNaN(CostsTotal2 / KwsTotal2) || ((CostsTotal2 / KwsTotal2) == 0) || !isFinite(CostsTotal2 / KwsTotal2)) { $('#UtilityWattCost2').val(""); } else { $('#UtilityWattCost2').val((CostsTotal2 / KwsTotal2).toFixed(2)); }

    //______________________________________________________________________________________________________

    //goto UsageTotals to calculate solar array page
    CalcSystemsize();
}

function CalcSystemsize() {
    $('#kWSystemSize').val(Math.ceil(parseFloat((($('#kWDailyAverage1').val() / 5.26)  * 1.17) * 100).toFixed(2)) / 100);
    CalcPanels();
}

function CalcPanels() {
    $('#NumberofPanels').val(Math.ceil((($('#kWSystemSize').val() * 1000) / $('#Panelwatts').val())));
    $('#kWSystemSize').val((($('#NumberofPanels').val() * $('#Panelwatts').val())) / 1000); //update system size based on num of panels
    $('#MonthlyGeneratedAvg').val(Math.ceil(((($('#NumberofPanels').val() * $('#Panelwatts').val() * 5.26) * .83) / 1000) * 30.4166666));
    $('#DailyGeneratedAvg').val(Math.ceil(parseFloat(($('#MonthlyGeneratedAvg').val() / 30.4166666) * 10).toFixed(1)) / 10);

    $('#SystemCost').val((Math.ceil(((($('#kWSystemSize').val() * 1000) * $('#SolarWattCost').val()) + parseInt($('#FinancingFee').val())))));
    $('#FederalCredit').val(Math.ceil($('#SystemCost').val() * .30));
    $('#FreeYearSavings').val(Math.ceil($('#EstYrCostsTotal1').val() * 1.072));
    $('#NetSystemCost').val(Math.ceil($('#SystemCost').val() - $('#StateCredit').val() - $('#FederalCredit').val() - ($('#EstYrCostsTotal1').val() * 1.072)));

    var principal = ($('#NetSystemCost').val());
    var interest = ($('#BankInterestRate').val() / 100 / 12);
    var payments = ($('#BankLoanYears').val() * 12);

    var x = Math.pow(1 + interest, payments);
    var monthly = (principal * x * interest) / (x - 1);

    $('#MonthlyPayment').val(parseFloat(monthly).toFixed(2));
    //  $('#totalPayment').val(monthly * payments);
    //  $('#interestPayment').val((monthly * payments) - principal);

    $('#TreesPlantedAnually').val(Math.ceil(parseFloat(($('#kWSystemSize').val() * 23.4))));
    $('#TotalTreesPlanted').val(Math.ceil(parseFloat(($('#kWSystemSize').val() * 585))));
    $('#CarsoffRoadAnnually').val(Math.ceil(parseFloat(($('#kWSystemSize').val() * .54))));
    $('#TotalCarsoffRoad').val(Math.ceil(parseFloat(($('#kWSystemSize').val() * 13.5))));

    var monthlyReplacedElecCost = (($('#kWSystemSize').val() * .8 * 6 * 365 / 12 * $('#UtilityWattCost1').val()) * (1.11)); // should add plus led light savings 
    var mSavings = ($('#CostsAvg1').val() - $('#MonthlyPayment').val());

    var presentVal = monthlyReplacedElecCost;

    var futureValue = 0;
    var curValue = (monthlyReplacedElecCost * 12);
    for (i = 1; i <= (15) ; i++) { futureValue = (futureValue + (curValue * (1 + ($('#annualincrease1').val() / 100)))); }

    //$('#MonthlySavingsw').val(Math.ceil(((((monthlyReplacedElecCost - (parseFloat($('#MonthlyPayment').val()))))) * 100).toFixed(2)) / 100);
    //$('#TotalUtilitySavings').val(Math.ceil(futureValue));
    //$('#HomeValueIncrease').val(Math.ceil(monthlyReplacedElecCost * 240));

    $('#MonthlySavingsw').val(Math.ceil(((mSavings) * 100).toFixed(2)) / 100);
    $('#TotalUtilitySavings').val(Math.ceil(futureValue));
    $('#HomeValueIncrease').val(Math.ceil($('#kWSystemSize').val() * 5911)); //research shows an average increase in resale value being $5,911 for each 1 kilowatt (kW) of solar installed.




    //update estimate page
    $('#kWSystemSizeest').html($('#kWSystemSize').val());
    $('#SystemCostest').html($('#SystemCost').val());
    $('#FederalCreditest').html($('#FederalCredit').val());
    $('#StateCreditest').html($('#StateCredit').val());
    $('#FreeYearSavingsest').html($('#FreeYearSavings').val());
    $('#NetSystemCostest').html($('#NetSystemCost').val());
    $('#MonthlySavingswest').html($('#MonthlySavingsw').val());
    $('#TotalUtilitySavingsest').html($('#TotalUtilitySavings').val());
    $('#HomeValueIncreaseest').html($('#HomeValueIncrease').val());
    $('#TreesPlantedAnuallyest').html($('#TreesPlantedAnually').val());
    $('#TotalTreesPlantedest').html($('#TotalTreesPlanted').val());
    $('#CarsoffRoadAnnuallyest').html($('#CarsoffRoadAnnually').val());
    $('#TotalCarsoffRoadest').html($('#TotalCarsoffRoad').val());

    //show cusomer info on estimate
    $('#panel-estimate').html("Estimate for " + $('#CustomerFirstName').val() + " " + $('#CustomerLastName').val() + " - " + $('#CustomerAddress').val() + ", " + $('#CustomerCity').val() + " - " + $('#CustomerPhone').val());

    codeAddress(); //update map address for available space
}

// lehiwilkerson asdfgh1 rm
//
// https://www.rockymountainpower.net/wum/signin.wcssn.html?cams_login_config=http&cams_original_url=http%3A%2F%2Fwww.rockymountainpower.net%2Fya%2Fvpb%2Fuh.wcss.html&cams_security_domain=system
// customer info and usage history
// https://www.rockymountainpower.net/ya/vpb/uh.wcss.html
function getusage() {
    $('button').attr('disabled', 'true')
    $(".spinner").show();
    $.ajax({
        type: "POST",
        url: '/WebService.asmx/' + $("#UtilityCompany").val(),
        data: JSON.stringify({ username: $('#SiteUsername').val(), password: $('#SitePassword').val() }),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            RockyMountainJSON = data.d;
            
            var fullname = RockyMountainJSON.customerName.split(' ');
            if (fullname.length > 2) {
                $('#CustomerFirstName').val(RockyMountainJSON.customerName.split(' ')[0]);
                $('#CustomerMiddleName').val(RockyMountainJSON.customerName.split(' ')[1]);
                $('#CustomerLastName').val(RockyMountainJSON.customerName.split(' ')[2]);
            } else {
                $('#CustomerFirstName').val(RockyMountainJSON.customerName.split(' ')[0]);
                $('#CustomerLastName').val(RockyMountainJSON.customerName.split(' ')[1]);
            }
            $('#CustomerAddress').val(RockyMountainJSON.customerAddress);
            $('#CustomerCity').val(RockyMountainJSON.customerCity);
            $('#Customerstate').val(RockyMountainJSON.customerState);
            $('#CustomerZip').val(RockyMountainJSON.customerZip);
        //    try {
            $('#CustomerPhone').val(RockyMountainJSON.customerPhone);
            $('#CustomerEmail').val(RockyMountainJSON.customerEmail);
      //      } catch (e) {   }
            $('#CustomerAccount').val(RockyMountainJSON.customerAccount);
            $('#CustomerMeter').val(RockyMountainJSON.customerMeter);

            for (var i = RockyMountainJSON.usage.length - 1; i >= 0; i--) {
                var usage = RockyMountainJSON.usage[i];
                var month = usage.date.split(' ')[0] + "2";
                if (i <= 11) { month = (usage.date.split(' ')[0]) }
                $('#' + month + 'Day').val(usage.days);
                $('#' + month + 'cost').val(usage.cost);
                $('#' + month + 'kw').val(usage.kw);
            }

            $('#graph').attr("src", "Content/graphs/" + RockyMountainJSON.customerMeter + ".jpg");
            $('#googmapimg').attr("src", "https://maps.googleapis.com/maps/api/staticmap?center=" + RockyMountainJSON.customerAddress + "," + RockyMountainJSON.customerCity + "," + RockyMountainJSON.customerState + "," + RockyMountainJSON.customerZip + "&zoom=20&size=230x160&maptype=satellite");
            //$('#googmapimgsame').attr("src", "https://maps.googleapis.com/maps/api/staticmap?center=" + RockyMountainJSON.customerAddress + "," + RockyMountainJSON.customerCity + "," + RockyMountainJSON.customerState + "," + RockyMountainJSON.customerZip + "&zoom=20&size=230x160&maptype=satellite");


            calculate();
            $('button').removeAttr('disabled', 'true')
            $(".spinner").hide();
        },
        error: function () {
            $('button').removeAttr('disabled', 'true')
            $(".spinner").hide();
        }
    });
}

function save() {
    var cusMeter = $('#CustomerLastName').val() + "_" + RockyMountainJSON.customerMeter;
    var myFirebaseRef = new Firebase("https://vivid-inferno-6002.firebaseio.com/" + cusMeter);

    //var fbobj = {};
    //fbobj[cusMeter] = {
    //    Name: {
    //        FirstName: $('#CustomerFirstName').val(),
    //        MiddleName: $('#CustomerMiddleName').val(),
    //        LastName: $('#CustomerLastName').val()
    //    },
    //    Address: {
    //        street: $('#CustomerAddress').val(),
    //        city: $('#CustomerCity').val(),
    //        state: $('#Customerstate').val(),
    //        zip: $('#CustomerZip').val()
    //    },
    //    Usage: RockyMountainJSON
    //};

    myFirebaseRef.set({
        Name: {
            FirstName: $('#CustomerFirstName').val(),
            MiddleName: $('#CustomerMiddleName').val(),
            LastName: $('#CustomerLastName').val()
        },
        Address: {
            street: $('#CustomerAddress').val(),
            city: $('#CustomerCity').val(),
            state: $('#Customerstate').val(),
            zip: $('#CustomerZip').val()
        },
        Account: {
            UserName: $('#SiteUsername').val(),
            Password: $('#SitePassword').val()
           
        },
        Usage: RockyMountainJSON
    });
}
//******************************************************************** map draw functions ***************************
function initialize() { // initiate a map
    geocoder = new google.maps.Geocoder();
    var GNSI = new google.maps.LatLng(37.422191, -122.084585); // 40.310130, -111.682423 this is orem's location just to initiate the 
    var mapOptions = {
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.HYBRID,
        center: GNSI
    }
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    buildDrawManager(); //goto load up menu for drawing
}

function codeAddress() { // update map center based on address
    google.maps.event.trigger($('#map-canvas'), 'resize'); // not sure if this is doing a refresh or any good
    var address = $('#CustomerAddress').val() + ", " + $('#CustomerCity').val() + ", " + $('#Customerstate').val();
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);

        } else {
            //  alert("Geocode was not successful for the following reason: " + status);
        }
    });
}

var buildDrawManager = function () {//load up menu for drawing and listen for events
    console.log('buildDrawManager');
    // Creates a drawing manager attached to the map that allows the user to draw
    // markers, lines, and shapes.
    drawingManager = new google.maps.drawing.DrawingManager({
        //drawingMode: google.maps.drawing.OverlayType.POLYGON, // sets default tool at start
        drawingControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT,
            drawingModes: [google.maps.drawing.OverlayType.POLYGON, google.maps.drawing.OverlayType.RECTANGLE]
        },
        polygonOptions: {strokeColor: "#000000",strokeOpacity: .9,strokeWeight: 2,fillColor: '#1E90F',strokeWeight: 0,fillOpacity: 0.4,editable: true,draggable: true,geodesic: false},
        rectangleOptions: { strokeColor: "#000000", strokeOpacity: .9, strokeWeight: 2, fillColor: '#1E90F', strokeWeight: 0, fillOpacity: 0.4, editable: true, draggable: true, geodesic: false },
        map: map
    });

    //listen for events
    google.maps.event.addListener(drawingManager, 'polygoncomplete', function (mapPolygon) { // Polygon was completed
        drawingManager.setDrawingMode(null);// Switch back to non-drawing mode after drawing a Polygon.
        calcs();     //don't end here keep listening
        google.maps.event.addListener(mapPolygon.getPath(), 'set_at', calcs) // Point was moved or removed or added
        google.maps.event.addListener(mapPolygon.getPath(), 'insert_at', calcs) // Point was moved or removed or added
        google.maps.event.addListener(mapPolygon, 'click', calcs) // Polygon was clicked or dragged
        google.maps.event.addListener(mapPolygon, 'dragend', calcs) // Polygon was clicked or dragged
        google.maps.event.addListener(mapPolygon, 'rightclick', function (mouseEvent) {mapPolygon.setMap(null);}); // Polygon was right clicked

        function calcs() { //Calculations and display 
            polygonArea = google.maps.geometry.spherical.computeArea(mapPolygon.getPath());
            sqmeter = polygonArea; sqft = (polygonArea * 10.7639); kwsys = (sqft * 0.013935469485967); panels = ((kwsys * 1000) / 260);
            document.getElementById("label1").innerHTML = "Area:  " + Math.ceil(sqft) + " SqFt " + "-   System: " + parseFloat(kwsys).toFixed(2) + " kW " + " -   Panels:  " + Math.ceil(panels);
            if (map.getTilt() != 0) {
                //do something if map is 45deg angled, maybe adjust calculations
            }
        }
    });//end polygon is completed _ end listening events

    google.maps.event.addListener(drawingManager, 'rectanglecomplete', function (rectangle) { // rectangle was completed
        drawingManager.setDrawingMode(null);// Switch back to non-drawing mode after drawing a rectangle.
        calcs();     //don't end here keep listening
      //google.maps.event.addListener(rectangle, "bounds_changed", calcs) // rectangle was resized moved
        google.maps.event.addListener(rectangle, 'click', calcs) // rectangle was clicked 
        google.maps.event.addListener(rectangle, 'rightclick', function (mouseEvent) { rectangle.setMap(null);});// rectangle was right clicked

        function calcs() { //Calculations and display 
            var bounds = rectangle.getBounds()
            polygonArea = google.maps.geometry.spherical.computeArea(bounds);
            alert(polygonArea);
            sqmeter = polygonArea; sqft = (polygonArea * 10.7639); kwsys = (sqft * 0.013935469485967); panels = ((kwsys * 1000) / 260);
            document.getElementById("label1").innerHTML = "Area:  " + Math.ceil(sqft) + " SqFt " + "-   System: " + parseFloat(kwsys).toFixed(2) + " kW " + " -   Panels:  " + Math.ceil(panels);
            if (map.getTilt() != 0) {
                //do something if map is 45deg angled, maybe adjust calculations
            }
        }
    });//end rectangle is completed _ end listening events




} //end of buildDrawManager function **********************************************************************************

//google.maps.event.addDomListener(window, 'load', initialize);

